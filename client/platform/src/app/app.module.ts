// AUTO GENERATED - DO NOT CHANGE MANUALLY
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { MartjackModules } from './martjack.modules';

import { AppComponents, AppRouterModule } from './app.router';

@NgModule({
    declarations: [
	AppComponent,
	...AppComponents
    ],
    imports: [
	BrowserModule,
	FormsModule,
	HttpModule,
	AppRouterModule,
	...MartjackModules
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
